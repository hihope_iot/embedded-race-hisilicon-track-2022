# 2022嵌入式/物联网竞赛海思赛道

#### 介绍
本仓用于2022嵌入式/物联网竞赛海思赛道的技术支持

#### 资料说明
| 资料     | 说明                       |
| -------- | -------------------------- |
| 硬件资料 | 竞赛套件的硬件相关资料说明 |
| 软件资料 | 环境搭建工具以及各种软件包 |

#### 视频资料

[Taurus&Pegasus AI计算机视觉基础开发套件组装说明](https://www.bilibili.com/video/BV1z3411n7zK/)

#### 厂测程序

###### 说明：

大赛套件出厂程序，可以测试Taurus套件的各种外设是否可用，并配有文档进行说明。

链接: https://pan.baidu.com/s/1EDOjvjz4BdzYoVa3tfKo0g 提取码: 5uny

