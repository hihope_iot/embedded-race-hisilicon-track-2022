# 使用方法

先下载官方的repo，命令如下：

```
repo init -u git@gitee.com:openharmony/manifest.git -b master --no-repo-verify
```


下载之后，打开当前目录的 .repo 文件夹，找到 manifests 文件夹下面的 default.xml 文件。替换成本仓库的 default.xml 文件，

接着继续后面的操作：

```
repo sync -c
repo forall -c 'git lfs pull'
```

