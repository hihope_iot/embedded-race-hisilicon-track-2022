# 2022嵌入式竞赛海思赛道

#### 介绍
Taurus Resource

### 目录说明

| 资料                 | 说明               |
| -------------------- | ------------------ |
| util_OHOSL1_3516.zip | 鸿蒙L1第三方的工具 |

#### 备注

resource（AI训练的资源文件）需要在Hihope官网上下载。

下载链接：http://www.hihope.org/download/download.aspx?mtt=49

