# 2022嵌入式竞赛海思赛道

#### 介绍
软件资料

#### 资料说明

| 资料                                | 说明                                              |
| ----------------------------------- | ------------------------------------------------- |
| HiSpark_Pegasus_Dev_Kit.V.1.0.0     | Pegasus智能家居套件所有外设样例代码               |
| HiSpark_Pegasus_OC_Kit_V1.0.0       | Pegasus智能家居套件LiteOS OceanConnect 样例代码   |
| HiSpark_Pegasus_Setup_Package01、02 | Liteos 开发环境搭建所需的工具，支持一键式环境搭建 |
| tools                               | 串口工具，OpenOCD调试工具等                       |

#### 环境搭建

1、下载该目录下HiSpark_Pegasus_Setup_Package01.zip和HiSpark_Pegasus_Setup_Package02.zip两个压缩包

2、解压之后，将HiSpark_Pegasus_Setup_Package02.zip目录下的software直接拷贝到HiSpark_Pegasus_Setup_Package01目录下

3、运行脚本：SetupInstall.bat