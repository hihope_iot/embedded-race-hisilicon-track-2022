### 已经搭建好的开发环境

建议大家使用docker，参考：https://gitee.com/hihope_iot/embedded-race-hisilicon-track-2022/blob/master/OpenHarmony%E6%95%99%E7%A8%8B%E6%96%87%E6%A1%A3/OpenHarmony%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E2%80%94docker.md 

### 视频课程：

（1）OpenHarmony环境搭建：https://www.bilibili.com/video/BV16R4y157xv

（2）烧录OpenHarmony系统：https://www.bilibili.com/video/BV16A4y197qM 

（3）应用开发入门：https://www.bilibili.com/video/BV1S34y1v7Zx


### 文档课程
https://gitee.com/hihope_iot/docs/tree/master/HiSpark-AI-Camera-Developer-Kit
