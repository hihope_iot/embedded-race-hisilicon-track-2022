# 2022嵌入式竞赛海思赛道

#### 介绍
Taurus & Pegasus AI计算机视觉基础开发套件

#### 目录说明
| 硬件资料                                               | 说明                                      |
| ------------------------------------------------------ | ----------------------------------------- |
| HiSpark_WiFi_IoT_外设扩展板_VER_A.pdf                  | Pegasus智能家居套件的外设原理图           |
| HiSpark_WiFi_IoT智能家居物联网开发套件_介绍            | Pegasus智能家居套件的硬件相关介绍         |
| HiSpark_WiFi_IoT智能家居开发套件_原理图                | Pegasus智能家居套件主板、底板等相关原理图 |
| HiSpark_WiFi_IoT套件PCB资料.zip                        | Pegasus智能家居套件PCB图                  |
| Wi-Fi_IoT套件_LiteOS扩展硬件指南及代码                 | 连接外设指南以及相关代码                  |
| Taurus套件原理图PCB设计资料                            | Taurus 套件原理图与PCB图                  |
| Taurus结构资料                                         | Taurus 套件结构资料                       |
| Taurus & Pegasus AI计算机视觉基础开发套件产品说明书    | 产品说明书                                |
| Taurus & Pegasus AI计算机视觉基础开发套件介绍-20210412 | 套件硬件相关介绍                          |
| Taurus & Pegasus AI计算机视觉开发套件组装说明          | 套件组装说明                              |

